import unittest
import company as cp

class Testing_Employee(unittest.TestCase):

    def setUp(self):
        self.emp1 = cp.Employee('Aamir', 'Ayub', 3000)

    def test_string_exceptions(self):
        self.assertRaises(ValueError, cp.Employee.validate_string, 1)
        self.assertRaises(ValueError, cp.Employee.validate_string)
        self.assertRaises(ValueError, cp.Employee.validate_string, ' ')
        self.assertRaises(ValueError, cp.Employee.validate_string, '')
        self.assertRaises(ValueError, cp.Employee.validate_string, 'abc')

    def test_numExceptions(self):
        self.assertRaises(ValueError, cp.Employee.validate_num)
        self.assertRaises(ValueError, cp.Employee.validate_num, '')
        self.assertRaises(ValueError, cp.Employee.validate_num, 's')
        self.assertRaises(ValueError, cp.Employee.validate_num, -1)
        self.assertRaises(ValueError, cp.Employee.validate_num, 5+1j)

    def test_exceptions(self):
        self.assertRaises(ValueError, setattr, self.emp1, 'first', 1)
        self.assertRaises(ValueError, setattr, self.emp1, 'first', ' ')
        self.assertRaises(ValueError, setattr, self.emp1, 'first', '')
        self.assertRaises(ValueError, setattr, self.emp1, 'first', 'abc')

        self.assertRaises(ValueError, setattr, self.emp1, 'last', 1)
        self.assertRaises(ValueError, setattr, self.emp1, 'last', ' ')
        self.assertRaises(ValueError, setattr, self.emp1, 'last', '')
        self.assertRaises(ValueError, setattr, self.emp1, 'last', 'abc')

    def test_names(self):
        self.assertEqual(self.emp1.first, 'Aamir')
        self.assertEqual(self.emp1.last, 'Ayub')

    def test_salary(self):
        self.assertEqual(self.emp1.salary, 3000)

    def test_raise(self):
        self.emp1.apply_raise()
        self.assertEqual(self.emp1.salary, 4200)



if __name__ == '__main__':
    unittest.main()