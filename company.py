class Employee:

    raise_amount = 1.4

    def __init__(self, first, last, salary):
        self.first = first
        self.last = last
        self.salary = salary
        self.email = f'{self.first}.{self.last}@company.com'

    @property
    def first(self):
        return self._first

    @first.setter
    def first(self, value=None):
        self._first = self.validate_string(value)

    @property
    def last(self):
        return self._last

    @last.setter
    def last(self, value=None):
        self._last = self.validate_string(value)

    @property
    def salary(self):
        return self._salary

    @salary.setter
    def salary(self, value):
        self._salary = self.validate_num(value)

    def apply_raise(self):
        self._salary = self._salary*self.raise_amount

<<<<<<< HEAD
    @classmethod
    def change_raise_amount(cls, value):
        cls.raise_amount = cls.validate_num(value)
=======
>>>>>>> parent of 70efe14... 9th

    @staticmethod
    def validate_string(value=None):
        if value is not None:
            if type(value) == str:
                if len(value.strip())<4:
                    raise ValueError('Input must has at least 4 characters')
                else:
                    return value
            else:
                raise ValueError('Input must be a string')
        else:
            raise ValueError('Input cannot be empty')

    @staticmethod
    def validate_num(value=None):
        if value is not None:
            if not isinstance(value, (int, float)) or value <= 0:
                raise ValueError('Input must be real number greater than 0')
            else:
                return value
        else:
            raise ValueError('Input cannot be empty')
